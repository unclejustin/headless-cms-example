import Vue from 'vue'
import VueMeta from 'vue-meta'
import App from './App.vue'
import { createRouter } from './router'
import '@/assets/css/tailwind.css'

Vue.config.productionTip = false

Vue.use(VueMeta)

export async function createApp({
  beforeApp = () => {},
  afterApp = () => {},
} = {}) {
  const router = createRouter()

  await beforeApp({
    router,
  })

  const app = new Vue({
    router,
    render: h => h(App),
  })

  const result = {
    app,
    router,
  }

  await afterApp(result)

  return result
}
