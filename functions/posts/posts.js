/* eslint-disable */
const fetch = require('node-fetch')

const GL_PRIVATE_TOKEN = process.env.GL_PRIVATE_TOKEN
const path = 'src/db/'.replace(/\//g, '%2F')

exports.handler = async function({ body }) {
  const { content } = JSON.parse(body)
  try {
    const response = await fetch(
      `https://gitlab.com/api/v4/projects/19821621/repository/files/${path}posts.json`,
      {
        method: 'PUT',
        body: JSON.stringify({
          commit_message: 'Update posts',
          branch: 'master',
          author_name: 'CMS',
          content,
        }),
        headers: {
          'CONTENT-TYPE': 'application/json',
          'PRIVATE-TOKEN': GL_PRIVATE_TOKEN,
        },
      },
    )
    if (!response.ok) {
      // NOT res.status >= 200 && res.status < 300
      return { statusCode: response.status, body: response.statusText }
    }
    const data = await response.json()

    return {
      statusCode: 200,
      body: JSON.stringify(data),
    }
  } catch (err) {
    console.log(err) // output to netlify function log
    return {
      statusCode: 500,
      body: JSON.stringify({ msg: err.message }), // Could be a custom message or object i.e. JSON.stringify(err)
    }
  }
}
